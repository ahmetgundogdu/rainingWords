(function(w) {
    w.MiniCanvasEngine = function(canvasElement, window, renderDelayMs, fullSize, autoPlay, renderFunction) {
        var s = this;

        s.canvas = canvasElement
        s.ctx = s.canvas.getContext("2d")
        s.window = window
        s.duration = renderDelayMs
        s.fullSize = fullSize
        s.autoPlay = autoPlay
        s.renderFunction = renderFunction

        s.lastRender = 0;

        s.play = function() {
            s.render()
        }

        s.stop = function() {
            s.render = false
        }

        s.render = function() {
            s.renderFunction(s, s.canvas, s.canvas.getContext("2d"))
            s.window.requestAnimationFrame(s.loopRender)
        }

        s.loopRender = function(timestamp) {
            s.progress = timestamp - s.lastRender
            if (s.rendering) {
                s.render()
            }
        }

        var init = (function(state) {

            let s = state

            if (s.autoPlay === true) {
                s.rendering = true
                s.play()
            } else {
                s.rendering = false
            }

            if (s.fullSize === true) {
                canvas.width = s.window.outerWidth
                canvas.height = s.window.outerHeight
            }

        })(this)

    };
})(window)