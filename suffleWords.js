var randInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

var canvas = document.getElementById('background')
var fsb = { min: 25, max: 25 }

var words = [
    { value: 'home' },
    { value: 'garden' },
    { value: 'pool' },
    { value: 'die' },
    { value: 'die' },
    { value: 'man' },
    { value: 'gold' },
    { value: 'howl' },
    { value: 'wolf' },
    { value: 'india' },
    { value: 'adjust' },
    { value: 'sight' },
    { value: 'real' },
    { value: 'protest' },
    { value: 'personal' },
    { value: 'loyalty' },
    { value: 'fog' },
    { value: 'agenda' },
    { value: 'value' },
    { value: 'pardon' },
    { value: 'rich' },
    { value: 'instruction' },
    { value: 'hell' },
    { value: 'producer' },
    { value: 'burial' },
    { value: 'auditor' }
]

var update = function (i = 0) {
    var fontSize = randInt(fsb.min, fsb.max),
        beforeWord = words[(i==0 ? (words.length - 1) : i - 1)]
        
    words[i].fontSize = fontSize



    words[i].y = randInt(-1 * canvas.height, typeof beforeWord.y != 'undefined' ? -1 * (beforeWord.fontSize) :  -i * 100 ); 

    words[i].y = -i * 200;

    words[i].x = randInt(0, (canvas.width - fontSize));
}

for(var i = 0;i < words.length;i++)
    update(i);

var engine = new MiniCanvasEngine(canvas, window, 0, true, true, function(state, canvas, ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var p = state.progress
    for (var i = 0; i < words.length; i++) {
        let fontSize = words[i].fontSize
        ctx.save();
        ctx.font = fontSize + 'px Arial';
        
        if(words[i].y <= (canvas.height + fontSize)) {
            words[i].y += 5
        } else {
            update(i)
        }
        ctx.fillStyle = "#FFF";
        ctx.fillText(words[i].value, words[i].x, words[i].y);
        ctx.restore();
    }
})